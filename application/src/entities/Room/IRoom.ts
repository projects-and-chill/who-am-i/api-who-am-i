export enum GameStep {
  preGame = "preGame",
  guesserSelection = "guesserSelection",
  wordSelection = "wordSelection",
  wordGuessing = "wordGuessing",
  scoreUpdating = "scoreUpdating",
  postGame = "postGame",
}

export interface IRoom {
  id: string
  name: string
  rules: {
    wordQty: number
    time: ["stopwatch" | "countdown", number]
    endGame: ["rounds" | "points", number]
    podium: "ascending" | "descending"
  }
  started: boolean
  protected: boolean
  open: boolean
  round: number
  teamTurn: string
  gameStep: GameStep
  avatar: string
  leader: string
  timeMaster: string
}
