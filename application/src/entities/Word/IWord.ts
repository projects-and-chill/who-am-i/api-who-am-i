export interface IWord {
  id: string
  value: string
  owner: string
  roomId: string
  guessed: boolean,
  validate: -1 | 0 | 1
  used: boolean,
}
