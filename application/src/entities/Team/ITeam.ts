export interface ITeam {
  id: string
  score: number
  playOrder: number
  played: boolean
  wordId: string
  color: string
  roomId: string
  guessedWords: string[]
}
