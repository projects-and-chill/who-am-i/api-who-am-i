export const healthchecking = async (req, res, next) => {

  try {

    res.status(200).send()

  }
  catch (error) {

    return next(error)

  }

}
export const test1 = async (req, res, next) => {

  try {

    console.log("-------PING RECU------------")
    res.status(200).json({ title: "le titre", message: "recu Ok OK" })

  }
  catch (error) {

    return next(error)

  }

}
export const test2 = async (req, res, next) => {

  try {

    res.status(500).json({
      title: "j'envoi volontairement une erreur",
      message: "holala une erreur"
    })

  }
  catch (error) {

    return next(error)

  }

}

export const test3 = async (req, res, next) => {

  try {

    res.status(400).json({ title: "   Yo je peux tester la DB" })

  }
  catch (error) {

    return next(error)

  }

}
