import express from "express"
import { healthchecking, test1, test2, test3 } from "./test-controller"

const router = express.Router()

router.get("/", healthchecking)
router.get("/1", test1)
router.get("/2", test2)
router.get("/3", test3)

export { router as routerTest }
