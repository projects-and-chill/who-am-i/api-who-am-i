import { Server, Socket } from "socket.io"
import { IRoom } from "../../entities/Room/IRoom"
import { IPlayer } from "../../entities/Player/IPlayer"
import { playerEvents, roomEvents, teamEvents } from "../Events"
import { Room } from "../../entities/Room/Room"
import { Player } from "../../entities/Player/Player"
import { ioErrorHandler } from "../../errorHandlers/IoErrorHandler"
import { Team } from "../../entities/Team/Team"
import { ITeam } from "../../entities/Team/ITeam"

function initLobbyHandler (io: Server, socket: Socket) {

  const list = async (
    callback: (rooms: Pick<IRoom, "id" | "name" | "avatar">[]
  ) => void) => {
    try {
      const pickRoom = await Room.list()
      callback(pickRoom)
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  const createRoom = async (
    payload: { roomName: string; username: string },
    callback: (room: IRoom, team: ITeam, player: IPlayer) => void
  ) => {

    try {

      const { username, roomName } = payload
      await Player.create(socket.id, username)
      const room = await Room.create(roomName, socket.id)
      const team = await Team.create(room.id)
      const { player } = await Player.joinRoom(socket.id, room.id, {
        roomId: room.id,
        teamId: team.id
      })
      socket.join(room.id)

      callback(room, team, player)

      io.emit(roomEvents.server.CREATED, ...[room])

    }
    catch (err) {
      ioErrorHandler(err, socket)
    }

  }

  const joinRoom = async (
    roomId: string,
    username: string,
    callback: (room: IRoom, teams: ITeam[], players: IPlayer[]) => void
  ) => {
    try {

      const room = await Room.checkRoomState(
        roomId,
        { open: true },
        "Cannot Join this room"
      ) as IRoom

      const team = await Team.create(roomId)
      const player = await Player.create(socket.id, username, {
        teamId: team.id,
        roomId: roomId
      })
      socket.join(roomId)

      const players = await Player.list(roomId)
      const teams = await Team.list(roomId)

      callback(room, teams, players)
      socket.to(roomId).emit(teamEvents.server.CREATED, ...[team])
      socket.to(roomId).emit(playerEvents.server.JOINED, ...[username, player])

    }
    catch (err) {
      ioErrorHandler(err, socket)
    }

  }

  socket.on(roomEvents.client.LIST, list)
  socket.on(roomEvents.client.CREATE, createRoom)
  socket.on(playerEvents.client.JOIN, joinRoom)

}

export { initLobbyHandler }
