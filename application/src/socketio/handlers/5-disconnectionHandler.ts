import { Server, Socket } from "socket.io"
import { Player } from "../../entities/Player/Player"
import { Room } from "../../entities/Room/Room"
import { playerEvents, roomEvents, teamEvents } from "../Events"
import { Team } from "../../entities/Team/Team"
import { ioErrorHandler } from "../../errorHandlers/IoErrorHandler"
import { IPlayer } from "../../entities/Player/IPlayer"
import wordModel from "../../models/WordModel"
import { GameStep, IRoom } from "../../entities/Room/IRoom"
import { InGameHandler } from "./3-inGameHandler"

function disconnectionHandler (io: Server, socket: Socket) {

  const TurnEndDisconnection = async (teamId: string, roomId : string) => {

    try {
      const { room } = await Team.turnEnd(teamId, roomId)
      const roundIsFinished = await Room.isRoundFinished(roomId)
      await Team.delete(teamId)
      io.to(roomId).emit(teamEvents.server.DELETED, ...[teamId])
      /*---Case: Round continue ---*/
      if (!roundIsFinished){
        await InGameHandler.gameNextTeam(io, socket)(roomId)
        return
      }
      const gameIsFinished : boolean = await Room.gameIsFinished(roomId, "all")

      /*---Case: End Game---*/
      if (gameIsFinished){
        const roomEndGame = await Room.goToStep(GameStep.postGame, roomId)
        io.to(roomId).emit(roomEvents.server.STEP_CHANGED, ...[roomEndGame])
        await InGameHandler.stepUpdateSideEffect(io, socket)(room)
        return
      }

      /*---Case: New Round---*/

      const teams = await Team.resetPlayed(roomId)
      const roomNewRound = Room.nextRound(roomId)
      io.to(roomId).emit(roomEvents.server.NEXT_ROUND, ...[roomNewRound, teams])

      await InGameHandler.gameNextTeam(io, socket)(roomId)
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  const leaveRoom = async () => {
    try {
      const deletedPlayer: IPlayer | null = await Player.delete(socket.id)
      if (!deletedPlayer)
        return

      const { roomId, teamId, username, id: playerId } = deletedPlayer

      const roomIsDeleted: boolean = await Room.checkRoomPlayers(roomId)
      const teamShouldDelete: boolean = await Team.checkTeamPlayers(teamId, false)
      socket.leave(roomId)

      if (roomIsDeleted) {
        await wordModel.deleteMany({ roomId })
        return io.emit(roomEvents.server.DELETED, ...[roomId])
      }

      io.to(roomId).emit(playerEvents.server.LEAVED, ...[username, playerId])
      const room = await Room.checkRoomState(
        roomId,
        {},
        "Unplained Error: Room not exist") as IRoom
      if (room.leader === socket.id) {
        const room = await Room.changeLeader(roomId)
        io.to(roomId).emit(roomEvents.server.LEADER_CHANGE, ...[room])
      }
      if (teamShouldDelete) {
        if (room.teamTurn === teamId) {
          await TurnEndDisconnection(teamId, roomId)
        }
        else {
          await Team.delete(teamId)
          io.to(roomId).emit(teamEvents.server.DELETED, ...[teamId])
        }
      }

    }
    catch (err) {
      ioErrorHandler(err, socket)
    }

  }

  socket.on(playerEvents.client.LEAVE, leaveRoom)
  socket.on("disconnect", leaveRoom)

}

export { disconnectionHandler }
