import { Server, Socket } from "socket.io"
import { playerEvents, roomEvents, teamEvents, wordEvent } from "../Events"
import { Team } from "../../entities/Team/Team"
import { Player } from "../../entities/Player/Player"
import { ioErrorHandler } from "../../errorHandlers/IoErrorHandler"
import { GameStep, IRoom } from "../../entities/Room/IRoom"
import { Room } from "../../entities/Room/Room"
import { Word } from "../../entities/Word/Word"
import playerModel from "../../models/PlayerModel"
import { IPlayer } from "../../entities/Player/IPlayer"
import { Chrono } from "../../entities/Chrono/Chrono"

function initPreGameHandler (io: Server, socket: Socket) {

  const createTeam = async (roomId: string) => {

    try {

      const team = await Team.create(
        roomId,
        { open: true, started: false },
        { id: socket.id }
      )
      const { prevTeamId, player } = await Player.switchTeam(socket.id, team.id, roomId)
      const teamDemeted: boolean = await Team.checkTeamPlayers(prevTeamId)

      if (teamDemeted)
        io.to(roomId).emit(teamEvents.server.DELETED, ...[prevTeamId])
      io.to(roomId).emit(teamEvents.server.CREATED, ...[team])
      io.to(roomId).emit(playerEvents.server.TEAM_SWITCHED, ...[player])

    }
    catch (err) {

      ioErrorHandler(err, socket)

    }

  }

  const switchTeam = async (teamId: string, roomId: string) => {

    try {
      await Room.checkRoomState(
        roomId,
        { open: true },
        "Cannot change team: game will start soon"
      )
      const { player, prevTeamId } = await Player.switchTeam(socket.id, teamId, roomId)
      const teamDeleted = await Team.checkTeamPlayers(prevTeamId)

      if (teamDeleted)
        io.to(roomId).emit(teamEvents.server.DELETED, ...[prevTeamId])
      io.to(roomId).emit(playerEvents.server.TEAM_SWITCHED, ...[player])

    }
    catch (err) {

      ioErrorHandler(err, socket)

    }

  }

  const editRules = async (rule: Partial<IRoom["rules"]>, roomId: string) => {

    try {
      const room = await Room.editRules(roomId, rule, socket.id)
      socket.to(roomId).emit(roomEvents.server.RULES_EDITED, ...[room.rules])
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }

  }

  const closeRoom = async (roomId: string) => {

    try {
      const room = await Room.closeRoomAccess(roomId, socket.id)
      io.emit(roomEvents.server.CLOSED, ...[room])
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }

  }

  const addWord = async (
    word: string,
    roomId: string,
    callback: (accepted: string) => void
  ) => {

    try {

      await Room.checkRoomState(roomId, {
        started: false,
        open: false
      })
      const { enough } = await Word.add(word, roomId, socket.id, true)
      callback(word)

      if (!enough)
        return
      // Vote up if player is ready
      const player = await Player.vote(socket.id, 1)
      io.to(roomId).emit(playerEvents.server.VOTED, ...[player])
      const playersNotReady = await playerModel
        .find({
          roomId: roomId,
          vote: { $ne: 1 }
        })
        .count()

      if (playersNotReady)
        return

      const players: IPlayer[] = await Player.resetVotes(roomId)
      io.to(roomId).emit(playerEvents.server.RESET_VOTE, ...[players])
      await startGame(roomId)

    }
    catch (err) {
      ioErrorHandler(err, socket)
    }

  }

  const removeWord = async (word: string, roomId: string) => {

    try {

      await Room.checkRoomState(roomId, {
        started: false,
        open: false
      })
      const { playerUnready } = await Word.remove(word, roomId, socket.id)

      if (!playerUnready)
        return

      // Unready player if player was ready
      const player = await Player.vote(socket.id, 0)
      io.to(roomId).emit(playerEvents.server.VOTED, ...[player])

    }
    catch (err) {

      ioErrorHandler(err, socket)

    }

  }

  const startGame = async (roomId: string) => {
    try {
      await Room.editGameStep(GameStep.guesserSelection, roomId, {
        started: true,
        round: 1
      })
      const chrono = await Chrono.create(roomId)
      const teams = await Team.definePlayOrder(roomId)
      const { room } = await Team.nextTeam(roomId)
      io.to(roomId).emit(roomEvents.server.GAME_STARTED, ...[room, teams, chrono])
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }

  }

  socket.on(wordEvent.client.REMOVE, removeWord)
  socket.on(wordEvent.client.ADD, addWord)
  socket.on(roomEvents.client.CLOSE, closeRoom)
  socket.on(roomEvents.client.EDIT_RULES, editRules)
  socket.on(playerEvents.client.SWITCH_TEAM, switchTeam)
  socket.on(teamEvents.client.CREATE, createTeam)

}

export { initPreGameHandler }
