import debuger, { Debugger } from "debug"
import { ReasonPhrases } from "http-status-codes"
import { ApiError } from "../entities/ApiError"

const debug: Debugger = debuger("api:md_error")

function errorsHandler (error, req, res) {

  if (error instanceof ApiError) {

    debug("%O", error)
    res.status(error.status).send({
      title: error.title,
      message: error.message,
      status: error.status,
      instance: error.instance || "none",
      type: error.type || "none"
    })

  }
  else {
    console.log("unplanned error: ", error)
    res.status(ReasonPhrases.INTERNAL_SERVER_ERROR).json({
      status: error.status,
      message: error.message
    })

  }

}

export default errorsHandler
