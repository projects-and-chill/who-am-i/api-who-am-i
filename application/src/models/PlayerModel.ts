import { model, Schema } from "mongoose"
import { IPlayer } from "../entities/Player/IPlayer"

const stringValidator = (str) => {

  return typeof str === "string"

}

const schema = new Schema<IPlayer>({
  id: { type: String, required: true, unique: true },
  username: { type: String, required: true },
  roomId: { type: String, validate: stringValidator, default: "" },
  blinded: { type: Boolean, required: true },
  vote: { type: Number, required: true },
  teamId: { type: String, validate: stringValidator, default: "" },
  avatar: { type: String, required: true }
})

const playerModel = model<IPlayer>("players", schema)
export default playerModel
