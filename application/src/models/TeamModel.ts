import { model, Schema } from "mongoose"
import { ITeam } from "../entities/Team/ITeam"

const stringValidator = (str) => {

  return typeof str === "string"

}

const schema = new Schema<ITeam>({
  id: { type: String, required: true, unique: true },
  color: { type: String, required: true },
  score: { type: Number, required: true },
  playOrder: { type: Number, required: true },
  played: { type: Boolean, required: true },
  wordId: { type: String, validate: stringValidator, default: "" },
  roomId: { type: String, required: true },
  guessedWords: [{ type: String, required: true }]
})

const teamModel = model<ITeam>("teams", schema)
export default teamModel
