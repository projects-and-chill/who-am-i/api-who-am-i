import { model, Schema } from "mongoose"
import { IChrono } from "../entities/Chrono/IChrono"

const schema = new Schema<IChrono>({
  id: { type: String, required: true, unique: true },
  roomId: { type: String, required: true },
  defaultTime: { type: Number, required: true },
  type: { type: String, required: true },
  state: { type: String, required: true },
  startAt: { type: Number, required: true },
  milliseconds: { type: Number, required: true }

})

const roomModel = model<IChrono>("chronos", schema)
export default roomModel
